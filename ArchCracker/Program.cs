﻿using System;
using System.Windows.Forms;

namespace ArchCracker
{
    public struct settingStruct
    {
        //------------------------------------------------//
        /* открыто ли меню настроек
        возможно открытие только одного окна настроек
        */
        public bool isSettingOpen;
        //------------------------------------------------//
        // метод открытия: перебор или по словарю
        // false - перебор
        // true - по словарю
        public bool useMetod;
        //------------------------------------------------//
        // заглавные буквы
        public bool useCapitalLetters;
        // строчные буквы
        public bool useLowerCase;
        // цифры
        public bool useNumbers;
        // спецсимволы
        public bool useSpecialCharacters;
        // использовать пробел
        public bool useSpace;
        // использовать только определенные символы
        public bool useCertanCharacters;
        public String certanCharacters;
        // использовать слова
        public bool useWords;
        public String words;
        //------------------------------------------------//
        /* перебирать от определенной длины
        1 по умолчанию
        */
        public bool useLenghtFrom;
        public int LenghtFrom;
        //------------------------------------------------//
        /* перебирать до определенной длины
        -1 по умолчанию 
        -1 если неопрделенной длины
        */    
        public bool useLenghtTo;
        public int LenghtTo;
        //------------------------------------------------//
        // файл словаря
        public String useDictionary;
        
    }
    public static class CallBackMy
    {
        public delegate void callbackEvent(settingStruct what);

        public static callbackEvent callbackEventHandler;
        public static callbackEvent settingFormCallbackEventHandler;
    }

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
