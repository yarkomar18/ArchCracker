﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

namespace ArchCracker
{
    public partial class Form1 : Form 
    {
        settingStruct currentSetting = new settingStruct();
        //Координаты мышки
        private int x = 0; private int y = 0;

        public Form1()
        {
            // Добавляем обработчик события - который запустит функцию Reload
            CallBackMy.callbackEventHandler = new CallBackMy.callbackEvent(this.settingReload);
            InitializeComponent();
            settingInitialize();

            writeToConsole("Программа запущена");
            writeToConsole("Установлен метод перебора");
        }

        // настройки по умолчанию
        private void settingInitialize()
        {
            currentSetting.isSettingOpen = false; // окно настроек закрыто

            currentSetting.useMetod = false; // Перебор
                
            currentSetting.useCapitalLetters = true; // заглавные буквы
            currentSetting.useLowerCase = true; // строчные буквы
            currentSetting.useNumbers = true; // цифры
            currentSetting.useSpecialCharacters = true;
            currentSetting.useSpace = true;
            currentSetting.useCertanCharacters = false;
            currentSetting.certanCharacters = String.Empty;
            currentSetting.useWords = false;
            currentSetting.words = String.Empty;

            currentSetting.useLenghtFrom = false;
            currentSetting.LenghtFrom = 1;
            currentSetting.useLenghtTo = false;
            currentSetting.LenghtTo = -1;

            currentSetting.useDictionary = String.Empty;

        }

        // обновление настроек
        void settingReload(settingStruct param)
        {
            if (param.useMetod != currentSetting.useMetod)
            {
                if (!param.useMetod)
                    writeToConsole("Установлен метод перебора");
                else
                    writeToConsole("Установлен метод по словарю");
            }

            currentSetting = param;
           
            this.StartButton.Enabled = true;

        }

        // Нажатие кнопки мышки
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X; y = e.Y;
        }
        // Движение мышки
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Location = new System.Drawing.Point(this.Location.X + (e.X - x), this.Location.Y + (e.Y - y));

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!SuitableFormat(textBox1.Text, ".zip") || !Ionic.Zip.ZipFile.IsZipFile(textBox1.Text) || textBox1.Text == String.Empty)
            {
                writeToConsole("Не выбран архив, либо архив не существует");
                this.StartButton.Enabled = true;
                return;
            }
            System.Threading.Thread newThread;
            switch (currentSetting.useMetod)
            {
                case true: // по словарю
                    if (!SuitableFormat(currentSetting.useDictionary, ".txt"))
                    {
                        writeToConsole("не выбран txt файл");
                        currentSetting.useDictionary = String.Empty;
                        return;
                    }
                    newThread = new System.Threading.Thread(delegate () { BruteByDictionary(); });
                    newThread.Start();
                    break;

                case false: // перебор
                    newThread = new System.Threading.Thread(delegate () { Brute(); });
                    newThread.Start();
                    break;
            }
        }

        // формат задается в виде .формат
        private bool SuitableFormat(String _fileName, String _format)
        {
            System.Text.StringBuilder t = new System.Text.StringBuilder();
            t.Append("[");
            t.Append(_format);
            t.Append("]$");

            if (Regex.IsMatch(_fileName, t.ToString()))
                return true;
            else
                return false;
        }

        private void StartButtonAccesChange(bool _status)
        {
            this.StartButton.Enabled = _status;
        }

        public delegate void StartButtonAccesChangeAsync(bool _status);

        private void filesListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void filesListBox_DragDrop(object sender, DragEventArgs e)
        {
            // получение массива строк с именами перенесенных файлов
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            // имя файла
            String fileName = String.Empty;
            // выбор последнего файла из группы переданных
            foreach (String file in files)
                fileName = file;

            /* проверяется строка на начичие .zip в конце
            ^ - начало строки
            [^.zip] - все символы, кроме символа x
            + - одно или более вхождений
            $ - конец строки
            */
            if(SuitableFormat(fileName, ".zip"))
            {
                textBox1.Text = fileName;
                String t = "выбран файл ";
                t += textBox1.Text;
                writeToConsole(t);
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MinimizeButton_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!currentSetting.isSettingOpen)
            {
                currentSetting.isSettingOpen = true;
                this.StartButton.Enabled = false;

                Setting settingform = new Setting();
                settingform.Owner = this;

                CallBackMy.settingFormCallbackEventHandler(what: currentSetting);
                settingform.Show();
            }
        }

        public void writeToConsole(String _text)
        {
            System.Text.StringBuilder t = new System.Text.StringBuilder();
            t.Append(DateTime.Now.ToString());
            t.Append(" ");
            t.Append(_text);
            listBox1.Items.Add(t.ToString());
            t = null;
        }

        public delegate void writeToConsoleAsync(String _text);
        
        private bool openZip(String _password)
        {
            // Выходной файл после сжатия файла выше.
            string zipPath = textBox1.Text;

            int i = textBox1.Text.LastIndexOf(@"\");
            String fileName = textBox1.Text;
            fileName = fileName.Substring(i+1, fileName.Length - 5 - i);
            
            // Извлечь файл zip в папку.
            string extractPath = "C:/Users/Yaroslav/Desktop/";
            extractPath += fileName;
            
            Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath);
            
            // Текущий пароль к архиву
            zip.Password = _password;
            try
            {
                zip.ExtractAll(extractPath);
            }
            catch (Exception)
            {
                zip.Dispose();
                return false;
            }
            zip.Dispose();
            return true;
        }

        public String ReplaceCharInString(System.String str, int index, System.Char newSymb)
        {
            String testString = str;
            System.Text.StringBuilder builder = new System.Text.StringBuilder(testString);
            builder[index] = newSymb;
            testString = builder.ToString();
            builder = null; 
            return testString;
        }

        private System.String Gen(System.Collections.Generic.List<char> alphabet, int idx, int digits)
        {
            String ret = "";

            for(int i = 0; i < digits; i++)
                ret += alphabet[0];
            
            int alphas = alphabet.Count;
            while (digits != 0)
            {
                digits--;

                ret = ReplaceCharInString(ret, digits, alphabet[idx % alphas]);
                idx /= alphas;
            }
            return ret;
        }

        private System.Collections.Generic.List<char> GetAlphabet()
        {
            System.Collections.Generic.List<char> alphabet = new System.Collections.Generic.List<char>();
            System.Collections.Generic.List<char> alphabet_UPPER = new System.Collections.Generic.List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            System.Collections.Generic.List<char> alphabet_LOWWER = new System.Collections.Generic.List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            System.Collections.Generic.List<char> alphabet_NUM = new System.Collections.Generic.List<char>() { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
            System.Collections.Generic.List<char> alphabet_SYMBOLS = new System.Collections.Generic.List<char>() { '!', '"', '#', '$', '%', '&', '(', ')', '*', '+', ',', '-', '_', '№', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', ']', '{', '}', '^', '~', '/', '|' };
            Char space = ' ';

            if(currentSetting.useNumbers)
                alphabet.AddRange(alphabet_NUM);
            if (currentSetting.useLowerCase)
                alphabet.AddRange(alphabet_LOWWER);
            if (currentSetting.useCapitalLetters)
                alphabet.AddRange(alphabet_UPPER);
            if (currentSetting.useSpace)
                alphabet.Add(space);
            if (currentSetting.useSpecialCharacters)
                alphabet.AddRange(alphabet_SYMBOLS);
            if (currentSetting.useCertanCharacters)
            {
                alphabet.Clear();
                for (int i = 0; i < currentSetting.certanCharacters.Length; i++)
                    alphabet.Add(currentSetting.certanCharacters[i]);
            }
            return alphabet;
        }

// --- не трогать

        private void Brute()
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            String time = "затраченное время ";

            // Пароль
            String pas = "";
            
            // Проверка на отсуцтвие пароля
            if (openZip(pas))
            {
                //writeToConsole("Пароля нет");
                BeginInvoke(new writeToConsoleAsync(writeToConsole), "Пароля нет");
                return;
            }

            // Максимальная длина
            int LengthTo = currentSetting.LenghtTo;
            
            // Минимальная длина
            int LengthFrom = currentSetting.LenghtFrom-1;
            int maxflag = 0;
            if (currentSetting.useLenghtTo == false)
            {
                maxflag = 1;
                LengthTo = LengthFrom + 1;
            }

            // Получение алфавита
            System.Collections.Generic.List<char> alphabet = GetAlphabet();

            // Если алфвит пустой
            if(alphabet.Count == 0)
            {
                //writeToConsole("Не выбраны символы");
                BeginInvoke(new writeToConsoleAsync(writeToConsole), "Не выбраны символы");
                return;
            }

            int numbers = 1;
            int alphas = alphabet.Count;
            // -------------------------------- START --------------------------------- //
            // writeToConsole("Перебор начался");
            BeginInvoke(new writeToConsoleAsync(writeToConsole), "Перебор начался");
            BeginInvoke(new StartButtonAccesChangeAsync(StartButtonAccesChange), false);
            sw.Start();

            for (int i = LengthFrom; i < LengthTo; i++)
            {
                if (maxflag == 1 && LengthTo < Int32.MaxValue)
                    LengthTo++;

                numbers *= (int)Math.Pow(alphas, LengthFrom + 1); // на каждом шаге чисел в alphas раз больше
                for (int cur = 0; cur < numbers; cur++)
                {
                    pas = Gen(alphabet, cur, i + 1);
                    if (openZip(pas))
                    {
                        String t = "Пароль к архиву: ";
                        BeginInvoke(new writeToConsoleAsync(writeToConsole), t+pas);
                        sw.Stop();
                        time += (sw.ElapsedMilliseconds / 1000.0).ToString();
                        time += " с";
                        sw = null;
                        BeginInvoke(new writeToConsoleAsync(writeToConsole), time);
                        BeginInvoke(new StartButtonAccesChangeAsync(StartButtonAccesChange), true);
                        return;
                    }
                }
            }
                      
            sw.Stop();
            sw = null;
            BeginInvoke(new writeToConsoleAsync(writeToConsole), "Пароль не найден");
            BeginInvoke(new StartButtonAccesChangeAsync(StartButtonAccesChange), true);
            return;
        }

        private void BruteByDictionary()
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            String time = "затраченное время ";


            if (currentSetting.useDictionary == String.Empty)
            {
                BeginInvoke(new writeToConsoleAsync(writeToConsole), "Не выбран файл словаря");
                this.StartButton.Enabled = true;
                return;
            }

            try
            {
                // открытие файла со словарем
                System.IO.StreamReader s = new System.IO.StreamReader(currentSetting.useDictionary, System.Text.Encoding.Default);
                
                string pas;
                bool ok = false;
                BeginInvoke(new writeToConsoleAsync(writeToConsole), "Перебор начался");
                sw.Start();
                while ((((pas = s.ReadLine()) != null)) && ok == false)
                {
                    ok = openZip(pas);
                    if (ok)
                    {
                        String t = "Пароль к архиву: ";
                        BeginInvoke(new writeToConsoleAsync(writeToConsole), t+pas);
                        sw.Stop();
                        time += (sw.ElapsedMilliseconds / 1000.0).ToString();
                        time += " с";
                        BeginInvoke(new writeToConsoleAsync(writeToConsole), time);
                    }
                }
                if(!ok)
                    BeginInvoke(new writeToConsoleAsync(writeToConsole), "Пароль не найден");
            }
            catch (Exception e)
            {
                BeginInvoke(new writeToConsoleAsync(writeToConsole), "Файл не существует либо ошибка при открытии");
            }
            finally
            {
                sw = null;
                this.StartButton.Enabled = true;
            }
        }
    }
}
