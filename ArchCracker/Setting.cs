﻿using System;
using System.Windows.Forms;

namespace ArchCracker
{
    public partial class Setting : Form
    {
        settingStruct currentSetting = new settingStruct();

        public Setting()
        {
            CallBackMy.settingFormCallbackEventHandler = new CallBackMy.callbackEvent(this.settingReload);
            InitializeComponent();
        }

        // Копирование структуры настроек
        void settingReload(settingStruct param)
        {
            currentSetting = param;
            SettingInit();
        }

        // Восстановление предыдущих настроек
        void SettingInit()
        {
            // useMetod
            if (!currentSetting.useMetod)
                comboBox2.Text = "Перебор";
            else
                comboBox2.Text = "По словарю";

            // useCapitalLetters
            if (currentSetting.useCapitalLetters)
                checkBox1.Checked = true;
            else
                checkBox1.Checked = false;

            // useLowerCase
            if (currentSetting.useLowerCase)
                checkBox2.Checked = true;
            else
                checkBox2.Checked = false;

            // useNumbers
            if (currentSetting.useNumbers)
                checkBox3.Checked = true;
            else
                checkBox3.Checked = false;

            // useSpecialCharacters
            if (currentSetting.useSpecialCharacters)
                checkBox4.Checked = true;
            else
                checkBox4.Checked = false;

            // useSpace
            if (currentSetting.useSpace)
                checkBox5.Checked = true;
            else
                checkBox5.Checked = false;

            // useCertanCharacters
            if (currentSetting.useCertanCharacters)
                checkBox6.Checked = true;
            else
                checkBox6.Checked = false;

            // useWords
            if (currentSetting.useWords)
                checkBox7.Checked = true;
            else
                checkBox7.Checked = false;

            // certanCharacters
            textBox1.Text = currentSetting.certanCharacters;

            // words
            textBox2.Text = currentSetting.words;

            // useLenghtFrom
            if (currentSetting.useLenghtFrom)
                checkBox8.Checked = true;
            else
                checkBox8.Checked = false;

            // LenghtFrom
            textBox3.Text = currentSetting.LenghtFrom.ToString();

            // useLenghtTo
            if (currentSetting.useLenghtTo)
                checkBox9.Checked = true;
            else
                checkBox9.Checked = false;

            // LenghtTo
            textBox4.Text = currentSetting.LenghtTo.ToString();
            if (textBox4.Text == "-1")
                textBox4.Text = "неопределенно";

            // useDictionary
            textBox5.Text = currentSetting.useDictionary;

          
        }
        // Обновление настроек перед отправкой
        void SettingUpdate()
        {
            currentSetting.isSettingOpen = false;

            currentSetting.useMetod = (comboBox2.SelectedIndex == 1);

            currentSetting.useCapitalLetters = checkBox1.Checked;
            currentSetting.useLowerCase = checkBox2.Checked;
            currentSetting.useNumbers = checkBox3.Checked;
            currentSetting.useSpecialCharacters = checkBox4.Checked;
            currentSetting.useSpace = checkBox5.Checked;
            currentSetting.useCertanCharacters = checkBox6.Checked;
            currentSetting.certanCharacters = textBox1.Text;
            currentSetting.useWords = checkBox7.Checked;
            currentSetting.words = textBox2.Text;

            // Если не перевелось в число - снять отметку
            if (!Int32.TryParse(textBox3.Text, out currentSetting.LenghtFrom) || textBox3.Text == String.Empty)
                checkBox8.Checked = false;
            if (!checkBox8.Checked)
                currentSetting.LenghtFrom = 1;
            if (currentSetting.LenghtFrom < 1)
                currentSetting.LenghtFrom = 1;
            currentSetting.useLenghtFrom = checkBox8.Checked;

            // Если не перевелось в число - снять отметку
            if(!Int32.TryParse(textBox4.Text, out currentSetting.LenghtTo) || textBox4.Text == String.Empty)
                checkBox9.Checked = false;
            if (!checkBox9.Checked)
                currentSetting.LenghtTo = -1;
            if (currentSetting.LenghtTo < -1)
                currentSetting.LenghtTo = 1;
            if (currentSetting.LenghtTo <= currentSetting.LenghtFrom)
                currentSetting.LenghtTo = currentSetting.LenghtFrom + 1;
            currentSetting.useLenghtTo = checkBox9.Checked;

            currentSetting.useDictionary = textBox5.Text;
        }

        
        private void filesListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                e.Effect = DragDropEffects.All;
            }
        }

        private void filesListBox_DragDrop(object sender, DragEventArgs e)
        {

            // получение массива строк с именами перенесенных файлов
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            // имя файла
            String fileName = String.Empty;
            // выбор последнего файла из группы переданных
            foreach (String file in files)
                fileName = file;

            /* проверяется строка на начичие .zip в конце
            ^ - начало строки
            [^.txt] - все символы, кроме txt
            + - одно или более вхождений
            $ - конец строки
            */
        
            if (System.Text.RegularExpressions.Regex.IsMatch(fileName, "[.txt]$"))
            {
                textBox5.Text = fileName;
            }
        }
        
        //Координаты мышки
        private int x = 0; private int y = 0;

        // Нажатие мышки
        private void panel_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X; y = e.Y;
        }
        
        // Движение мышки
        private void panel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Location = new System.Drawing.Point(this.Location.X + (e.X - x), this.Location.Y + (e.Y - y));
            }
        }

        // Переключение панелей
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.comboBox1.SelectedIndex)
            {
                case 0:
                    panel1.Hide();
                    panel2.Hide();
                    panel3.Hide();
                    panel4.Show();
                    break;
                case 1:
                    panel4.Hide();
                    panel3.Hide();
                    panel2.Hide();
                    panel1.Show();
                    break;
                case 2:
                    panel4.Hide();
                    panel3.Hide();
                    panel1.Hide();
                    panel2.Show();
                    break;
                case 3:
                    panel4.Hide();
                    panel1.Hide();
                    panel2.Hide();
                    panel3.Show();
                    break;
                default:
                    panel1.Hide();
                    panel2.Hide();
                    panel3.Hide();
                    panel4.Hide();
                    break;
            }


        }

        private void checkBox8_CheckedChanged_1(object sender, EventArgs e)
        {
            if (this.checkBox8.Checked)
            {
                this.textBox3.Enabled = true;
            }
            else
            {
                this.textBox3.Enabled = false;
            }
        }

        private void checkBox9_CheckedChanged_1(object sender, EventArgs e)
        {
            if (this.checkBox9.Checked)
            {
                this.textBox4.Enabled = true;
            }
            else
            {
                this.textBox4.Enabled = false;
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox6.Checked)
            {
                this.textBox1.Enabled = true;
            }
            else
            {
                this.textBox1.Enabled = false;
            }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox7.Checked)
            {
                this.textBox2.Enabled = true;
            }
            else
            {
                this.textBox2.Enabled = false;
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void onSettingClosing(object sender, EventArgs e)
        {
            // сохранение текущих настроек
            SettingUpdate();
            // отправление настройек в главное окно
            CallBackMy.callbackEventHandler(what: currentSetting);
        }
        
    }
}
